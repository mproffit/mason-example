#include "JetSelectionHelper/JetSelectionHelper.h"
// stdlib functionality
#include <fstream>
#include <string>
#include <stdexcept>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <memory>
// ROOT functionality
#include <TFile.h>
#include <TH1.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/EgammaContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "AsgTools/MessageCheck.h"
// jet calibration
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"

namespace {

	struct Config {
		double min_pt = 50e3;
		double max_abs_eta = 2.5;
	};

	//Config get_config(const std::string pathname) {
	//	std::ifstream filestream(pathname);
	//	if (filestream.bad()) {
	//		throw std::runtime_error("unable to parse config");
	//	}
	//	std::string line;
	//	Config config;
	//	while (std::getline(filestream, line)) {
	//		const auto split_pos = line.find(':');
	//		if (split_pos == std::string::npos) {
	//			throw std::runtime_error("unable to parse config");
	//		}
	//		const auto name = line.substr(0, split_pos);
	//		if (name == "min_pt") {
	//			const auto value = std::stod(line.substr(split_pos + 1));
	//			config.min_pt = value;
	//		} else if (name == "max_abs_eta") {
	//			const auto value = std::stod(line.substr(split_pos + 1));
	//			config.max_abs_eta = value;
	//		} else {
	//			throw std::runtime_error("unable to parse config");
	//		}
	//	}
	//	return config;
	//}

}

using namespace asg::msgUserCode;

int main(const int argc, const char *const argv[]) {

  Config config;

  const char *input_pathname = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  if (argc > 1) {
    input_pathname = argv[1];
  }

  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;
  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");

  ANA_CHECK (JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMTopo"                                                  ));
  ANA_CHECK (JetCalibrationTool_handle.setProperty("ConfigFile"   ,"JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"));
  ANA_CHECK (JetCalibrationTool_handle.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear"                              ));
  ANA_CHECK (JetCalibrationTool_handle.setProperty("CalibArea"    ,"00-04-82"                                                       ));
  ANA_CHECK (JetCalibrationTool_handle.setProperty("IsData"       ,false                                                            ));

  ANA_CHECK (JetCalibrationTool_handle.retrieve());

  // initialize the xAOD EDM
  ANA_CHECK (xAOD::Init());

  // open the input file
  xAOD::TEvent event;
  std::unique_ptr<TFile> iFile(TFile::Open(input_pathname, "READ"));
  if ( ! iFile) {
    return 1;
  }
  ANA_CHECK (event.readFrom(iFile.get()));

  const char *output_file_path = "plots.root";

  // for counting events
  unsigned count = 0;

  JetSelectionHelper jet_helper;

  // get the number of events in the file to loop over
  Long64_t numEntries = -1;
  if (argc == 3) {
    numEntries = std::stol(argv[2]);
  } else if (argc > 3) {
    output_file_path = argv[2];
    numEntries = std::stol(argv[3]);
  }
  if (numEntries == -1) {
    numEntries = event.getEntries();
  }
  std::cout << "Processing " << numEntries << " events" << std::endl;

  TFile out_file(output_file_path, "RECREATE");
  if (out_file.IsZombie()) {
    throw std::runtime_error(std::string("Unable to open output ROOT file ") + std::string(output_file_path));
  }

  auto h_n_jets = new TH1F("n_jets", "Number of Jets;N Jets Per Event;N Events / Bin", 20, 0.0, 20.0);
  auto h_n_good_jets = new TH1F("n_good_jets", "Number of Jets;N Jets Per Event;N Events / Bin", 20, 0.0, 20.0);
  auto h_n_b_jets = new TH1F("n_b_jets", "Number of Jets;N Jets Per Event;N Events / Bin", 20, 0.0, 20.0);
  auto h_n_good_b_jets = new TH1F("n_good_b_jets", "Number of Jets;N Jets Per Event;N Events / Bin", 20, 0.0, 20.0);

  auto h_mjj = new TH1F("mjj", "Dijet Invariant Mass;Dijet Invariant Mass [GeV];N Events / Bin", 20, 0.0, 500.0);
  auto h_mjj_good = new TH1F("mjj_good", "Dijet Invariant Mass;Dijet Invariant Mass [GeV];N Events / Bin", 20, 0.0, 500.0);
  auto h_mjj_b = new TH1F("mjj_b", "Dijet Invariant Mass;Dijet Invariant Mass [GeV];N Events / Bin", 20, 0.0, 500.0);
  auto h_mjj_good_b = new TH1F("mjj_good_b", "Dijet Invariant Mass;Dijet Invariant Mass [GeV];N Events / Bin", 20, 0.0, 500.0);

  auto h_e_jet_delta_eta = new TH1F("e_jet_delta_eta", ";#Delta#eta;N Events / Bin", 100, 0.0, 6.0);
  auto h_e_jet_delta_phi = new TH1F("e_jet_delta_phi", ";#Delta#phi;N Events / Bin", 100, 0.0, 3.14159);

  // primary event loop
  for (Long64_t i = 0; i < numEntries; ++i) {

    // Load the event
    event.getEntry(i);

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo *ei = nullptr;
    ANA_CHECK (event.retrieve(ei, "EventInfo"));
    //std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;
    if (count % 1000 == 0) {
      std::cout << "Processed " << count << " events" << std::endl;
    }

    const auto mcEventWeight = ei->mcEventWeights()[0];

    // retrieve the jet container from the event store
    const xAOD::JetContainer *jets = nullptr;
    ANA_CHECK (event.retrieve(jets, "AntiKt4EMTopoJets"));

    const xAOD::EgammaContainer *electrons = nullptr;
    ANA_CHECK (event.retrieve(electrons, "Electrons"));

    const xAOD::MuonContainer *muons = nullptr;
    ANA_CHECK (event.retrieve(muons, "Muons"));

    //std::cout << "number of electrons: " << electrons->size() << std::endl;
    //std::cout << "number of muons: " << muons->size() << std::endl;

    h_n_jets->Fill(jets->size(), mcEventWeight);

    const xAOD::Jet *lead_jet = nullptr;
    const xAOD::Jet *lead_good_jet = nullptr;
    const xAOD::Jet *lead_b_jet = nullptr;
    const xAOD::Jet *lead_good_b_jet = nullptr;

    const xAOD::Jet *sublead_jet = nullptr;
    const xAOD::Jet *sublead_good_jet = nullptr;
    const xAOD::Jet *sublead_b_jet = nullptr;
    const xAOD::Jet *sublead_good_b_jet = nullptr;

    unsigned n_good_jets = 0;
    unsigned n_b_jets = 0;
    unsigned n_good_b_jets = 0;

    std::vector<std::unique_ptr<xAOD::Jet> > calibrated_jets;

    // loop through all of the jets and make selections with the helper
    for(const auto *const jet : *jets) {
      //if ( ! (jet->pt() >= config.min_pt && std::abs(jet->eta()) < config.max_abs_eta)) {

      // calibrate the jet
      {
        xAOD::Jet *jet_ptr;
        JetCalibrationTool_handle->calibratedCopy(*jet, jet_ptr);
        calibrated_jets.emplace_back(jet_ptr);
      }
      const auto *const calibrated_jet = calibrated_jets.back().get();

      const bool is_good_jet = jet_helper.isJetGood(calibrated_jet);
      const bool is_b_jet = jet_helper.isJetBFlavor(calibrated_jet);

      if (is_good_jet) {
        ++n_good_jets;
      }
      if (is_b_jet) {
        ++n_b_jets;
      }
      if (is_good_jet && is_b_jet) {
        ++n_good_b_jets;
      }

      for (const auto *const electron : *electrons) {
        const auto delta_eta = std::abs(electron->eta() - calibrated_jet->eta());
        const auto delta_phi = std::abs(electron->p4().DeltaPhi(calibrated_jet->p4()));
        h_e_jet_delta_eta->Fill(delta_eta, mcEventWeight);
        h_e_jet_delta_phi->Fill(delta_phi, mcEventWeight);
      }

      //// print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;

      if (lead_jet == nullptr || calibrated_jet->pt() > lead_jet->pt()) {
        if (lead_jet != nullptr) {
          sublead_jet = lead_jet;
        }
        lead_jet = calibrated_jet;
      } else if (sublead_jet == nullptr || calibrated_jet->pt() > sublead_jet->pt()) {
        sublead_jet = calibrated_jet;
      }

      if (is_good_jet) {
        if (lead_good_jet == nullptr || calibrated_jet->pt() > lead_good_jet->pt()) {
          if (lead_good_jet != nullptr) {
            sublead_good_jet = lead_good_jet;
          }
          lead_good_jet = calibrated_jet;
        } else if (sublead_good_jet == nullptr || calibrated_jet->pt() > sublead_good_jet->pt()) {
          sublead_good_jet = calibrated_jet;
        }
      }

      if (is_b_jet) {
        if (lead_b_jet == nullptr || calibrated_jet->pt() > lead_b_jet->pt()) {
          if (lead_b_jet != nullptr) {
            sublead_b_jet = lead_b_jet;
          }
          lead_b_jet = calibrated_jet;
        } else if (sublead_b_jet == nullptr || calibrated_jet->pt() > sublead_b_jet->pt()) {
          sublead_b_jet = calibrated_jet;
        }
      }

      if (is_good_jet && is_b_jet) {
        if (lead_good_b_jet == nullptr || calibrated_jet->pt() > lead_good_b_jet->pt()) {
          if (lead_good_b_jet != nullptr) {
            sublead_good_b_jet = lead_good_b_jet;
          }
          lead_good_b_jet = calibrated_jet;
        } else if (sublead_good_b_jet == nullptr || calibrated_jet->pt() > sublead_good_b_jet->pt()) {
          sublead_good_b_jet = calibrated_jet;
        }
      }
    }

    h_n_good_jets->Fill(n_good_jets, mcEventWeight);
    h_n_b_jets->Fill(n_b_jets, mcEventWeight);
    h_n_good_b_jets->Fill(n_good_b_jets, mcEventWeight);

    if (lead_jet && sublead_jet) {
      h_mjj->Fill((lead_jet->p4() + sublead_jet->p4()).M() / 1000.0, mcEventWeight);
    }
    if (lead_good_jet && sublead_good_jet) {
      h_mjj_good->Fill((lead_good_jet->p4() + sublead_good_jet->p4()).M() / 1000.0, mcEventWeight);
    }
    if (lead_b_jet && sublead_b_jet) {
      h_mjj_b->Fill((lead_b_jet->p4() + sublead_b_jet->p4()).M() / 1000.0, mcEventWeight);
    }
    if (lead_good_b_jet && sublead_good_b_jet) {
      h_mjj_good_b->Fill((lead_good_b_jet->p4() + sublead_good_b_jet->p4()).M() / 1000.0, mcEventWeight);
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  out_file.cd();

  h_n_jets->Write();
  h_n_good_jets->Write();
  h_n_b_jets->Write();
  h_n_good_b_jets->Write();

  h_mjj->Write();
  h_mjj_good->Write();
  h_mjj_b->Write();
  h_mjj_good_b->Write();

  h_e_jet_delta_eta->Write();
  h_e_jet_delta_phi->Write();

  // exit from the main function cleanly
  return 0;
}

